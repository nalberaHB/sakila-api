import express from 'express';
import getAllCustomersController from '../controllers/customer/getAllCustomersController.js';


const router = express.Router();


router.get('/customers', getAllCustomersController);


export default router;