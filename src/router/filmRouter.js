import express from 'express';

import getFilmByIdController from '../controllers/films/getFilmByIdController.js';

const router = express.Router();

router.get('/film/:id', getFilmByIdController);

export default router;