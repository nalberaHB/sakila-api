import express from 'express';
import getAllActorsController from '../controllers/actors/getAllActorsController.js';
import getActorByIdController from '../controllers/actors/getActorByIdController.js';
import insertNewActorController from '../controllers/actors/insertNewActorController.js';
import modifyActorByIdCotroller from '../controllers/actors/modifyActorByIdCotroller.js';
import deleteActorController from '../controllers/actors/deleteActorController.js';

const router = express.Router();


router.get('/actors', getAllActorsController);
router.get('/actors/:id', getActorByIdController);

router.post('/actors/new-actor', insertNewActorController);

router.put('/actors/modify/:id', modifyActorByIdCotroller);

router.delete('/actors/:id', deleteActorController);

export default router;