import Film from "../../database/models/Film.js";
import Actors from "../../database/models/Actors.js";

const getFilmByIdController = async (req,res) => {
    try {
        
        const { id } = req.params;

        const film = await Film.findByPk(id, {
            include: [
                {model: Actors}
            ]
        });

        res.send({
            status: 'ok',
            data: film
        })
    } catch (error) {
        console.log(error);
    }
}

export default getFilmByIdController;