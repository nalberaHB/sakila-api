import Actors from "../../database/models/Actors.js";

const modifyActorByIdCotroller = async (req,res) => {
    try {
        
        //tomamos los datos que nos envían por body
        const { first_name, last_name } = req.body;
        const { id } = req.params;

        await Actors.update(
            //primer parámetro: el objeto con los datos a actualizar
            {
                first_name,
                last_name,
                last_update: new Date()
            },
            //segundo parámetro el WHERE
            {
                where:{
                    actor_id: id
                }
            }
        );

        //retornamos al cliente la información en caso de que haya resultado satisfactorio
        res.send({
            status: 'ok',
            message: 'Successfully modified actor...',
        });

    } catch (error) {
        console.log(error);
    }
}

export default modifyActorByIdCotroller;