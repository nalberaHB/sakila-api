import Actors from "../../database/models/Actors.js";
import Film from "../../database/models/Film.js";

const getActorByIdController = async (req,res) => {
    try {
        
        const { id } = req.params;

        const actor = await Actors.findByPk(id,{
            include: [
                {model: Film}
            ]
        });

        if(!actor) return res.send({status: 'error', message: 'Actor no encontrado'});

        res.send({
            status: 'ok',
            data: actor
        })

    } catch (error) {
        console.log(error);
    }
}

export default getActorByIdController;