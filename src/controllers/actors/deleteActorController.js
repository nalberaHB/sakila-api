import Actors from "../../database/models/Actors.js";

const deleteActorController = async (req,res) => {
    try {
        
        const { id } = req.params;

        await Actors.destroy({
            where:{
                actor_id: id
            }
        });

        res.send({
            status: 'ok',
            message: 'Successfully deleted actor...',
        });

    } catch (error) {
        console.log(error);
    }
}

export default deleteActorController;