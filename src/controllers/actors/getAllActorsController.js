import { Op } from 'sequelize';

import Actors from '../../database/models/Actors.js';

const getAllActorsController = async (req,res) => {
    
    try {
        
        //const actors = await Actors.findAll(); //select * from actor
        
        //selct a.first_name, a.last_name from actor a

        const { name } = req.query;

        if(name){
            const actor = await Actors.findAll({
                attributes: ['first_name','last_name'],
                where:{
                    first_name: {
                        [Op.like]: `%${name}%`
                    }
                },
                order: [
                    ['last_name', 'ASC'],
                ]
                
            });

            if(actor.length){
                return res.send({
                    status: 'ok',
                    data: actor
                });      
            }else{
                return res.send({
                    status: 'not found',
                    message: 'No existen coincidencias'
                })
            }

        }

        const actors = await Actors.findAll({
            attributes: ['actor_id','first_name','last_name'],
            // order: [
            //     ['last_name', 'ASC']
            // ],
            limit: 10,
            offset: 10
        });

        res.send({
            status: 'ok',
            data: actors
        });

    } catch (error) {
        console.log(error);
    }
    
}

export default getAllActorsController;