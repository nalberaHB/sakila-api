import Actors from "../../database/models/Actors.js";

const insertNewActorController = async (req,res) => {
    try {
        
        const {first_name, last_name} = req.body;

        const newActor = await Actors.create({
            first_name,
            last_name
        });

        res.send({
            status: 'ok',
            message: 'Actor creado correctamente',
            data: newActor
        })

    } catch (error) {
        console.log(error);
    }
}

export default insertNewActorController;