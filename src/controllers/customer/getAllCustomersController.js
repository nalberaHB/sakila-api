import Customer from "../../database/models/Customer.js";
import Address from "../../database/models/Address.js";

const getAllCustomersController = async (req,res) => {
    try {
        
        const customers = await Customer.findAll({
            attributes: ['customer_id','last_name','first_name','email'],
            //join
            include: [
                {model: Address, attributes: ['address', 'district', 'postal_code']}
            ]
        })

        res.send({
            status: 'ok',
            data: customers
        });

    } catch (error) {
        console.log(error);
    }
}

export default getAllCustomersController;