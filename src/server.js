import express from 'express';
import actorRouter from '../src/router/actorRouter.js';
import customerRouter from '../src/router/customerRouter.js';
import filmRouter from '../src/router/filmRouter.js';

const server = express();

server.use(express.json());

server.use(actorRouter);
server.use(customerRouter);
server.use(filmRouter);

export default server;