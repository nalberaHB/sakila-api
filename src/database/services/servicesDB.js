import connection from "../config/configDB.js";
import Actors from "../models/Actors.js";
import Address from "../models/Address.js";
import Customer from "../models/Customer.js";
import Film from "../models/Film.js";
import FilmActor from "../models/FilmActor.js"

export const testConnection = async () => {
    try {
        
        await connection.authenticate();
        console.log(`Conectado a base de datos ${connection.getDatabaseName()}`);
    } catch (error) {
        console.log(error);
    }
}

export const syncDB = async () => {

    associationsModels();
    await Actors.sync({alter: true});
    await Address.sync({alter: true});
    await Customer.sync({alter: true});
    await Film.sync({alter: true});
    await FilmActor.sync({alter: true});
}

const associationsModels = () => {

    Customer.belongsTo(Address, {foreignKey: 'address_id'});
    Address.hasMany(Customer, {foreignKey: 'address_id'});

    Actors.belongsToMany(Film, {
        through: 'film_actor', //a través de que tabla debe hacerse (tabla intermedia)
        foreignKey: 'actor_id', //clave foránea de la tabla film que hace referencia a actor
        otherKey: 'film_id', // referencia a la tabla intermedia
        timestamps: false // aclaramos si la tabla pivote tiene o no campos timestamp
    });

    Film.belongsToMany(Actors, {
        through: 'film_actor',
        foreignKey: 'film_id',
        otherKey: 'actor_id',
        timestamps: false
    });

    //si la base de datos no existen el resto de tablas ni tienen datos
    //se puede hacer de esta forma
    //Actors.belongsToMany(Film, {through: 'filmXactor'});
    //Film.belongsToMany(Actors, {through: 'filmXactor'})

}