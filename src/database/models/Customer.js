import { DataTypes, Model, Sequelize } from "sequelize";
import connection from "../config/configDB.js";


class Customer extends Model {};

Customer.init({
    customer_id:{
        type: DataTypes.INTEGER.UNSIGNED,
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    store_id: {
        type: DataTypes.TINYINT.UNSIGNED,
        allowNull: true
    },
    first_name:{
        type: DataTypes.STRING,
        allowNull: true
    },
    last_name:{
        type: DataTypes.STRING,
        allowNull: true
    },
    email:{
        type: DataTypes.STRING,
        defaultValue: null
    },
    address_id:{
        type: DataTypes.SMALLINT.UNSIGNED,
        allowNull: true
    },
    active:{
        type: DataTypes.TINYINT,
        allowNull: true,
        defaultValue: 1
    },
    create_date:{
        type: DataTypes.DATE
    },
    last_update:{
        type: DataTypes.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')
    }
    },
    {
        tableName: 'customer',
        timestamps: false,
        sequelize: connection
    }
)

export default Customer;