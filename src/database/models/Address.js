import { DataTypes, Model, Sequelize } from "sequelize";
import connection from "../config/configDB.js";


class Address extends Model {};

Address.init({
    address_id:{
        type: DataTypes.SMALLINT.UNSIGNED,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    address:{
        type: DataTypes.STRING,
        allowNull: false
    },
    address2:{
        type: DataTypes.STRING,
        defaultValue: null
    },
    district:{
        type: DataTypes.STRING,
        allowNull: false
    },
    city_id:{
        type: DataTypes.SMALLINT.UNSIGNED,
        allowNull: false,
    },
    postal_code:{
        type: DataTypes.STRING,
        defaultValue: null
    },
    phone:{
        type: DataTypes.STRING,
        allowNull: false
    },
    locatios:{
        type: DataTypes.GEOMETRY,
    },
    last_update:{
        type: DataTypes.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')
    }
    },
    {
        tableName: 'address',
        timestamps: false,
        sequelize: connection
    }
)

export default Address;
