import { DataTypes, Model, Sequelize } from "sequelize";
import connection from "../config/configDB.js";

class Film extends Model {};


Film.init({
    film_id:{
        type: DataTypes.SMALLINT.UNSIGNED,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    title:{
        type: DataTypes.STRING,
        allowNull: false
    },
    description:{
        type: DataTypes.TEXT,
        defaultValue: null
    },
    release_year:{
        type: DataTypes.STRING,
        defaultValue: null
    },
    languaje_id:{
        type: DataTypes.TINYINT.UNSIGNED,
        allowNull: false
    },
    original_language_id:{
        type: DataTypes.TINYINT.UNSIGNED,
        defaultValue: null
    },
    rental_duration:{
        type: DataTypes.TINYINT.UNSIGNED,
        allowNull: false,
        defaultValue: 3
    },
    rental_date:{
        type: DataTypes.DECIMAL(4,2),
        allowNull: false,
        defaultValue: 4.99
    },
    lenght:{
        type: DataTypes.SMALLINT.UNSIGNED,
        defaultValue: null
    },
    replacement_cost:{
        type: DataTypes.DECIMAL(5,2),
        allowNull: false,
        defaultValue: 19.99
    },
    rating:{
        type: DataTypes.ENUM,
        values: ['G', 'PG', 'PG-13', 'R', 'NC-17'],
        defaultValue: 'G'
    },
    // special_features:{
    //     type: DataTypes.ENUM,
    //     values: ['Trailers','Commentaries','Deleted Scenes','Behind the Scenes'],
    //     defaultValue: null
    // },
    last_update:{
        type: DataTypes.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
        allowNull: false
    }
    },
    {
        tableName: 'film',
        timestamps: false,
        sequelize: connection
    }
)

export default Film;