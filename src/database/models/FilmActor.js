import { DataTypes, Model, Sequelize } from "sequelize";
import connection from "../config/configDB.js";


class FilmActor extends Model {};

FilmActor.init({
    actor_id:{
        type: DataTypes.SMALLINT.UNSIGNED,
        primaryKey: true,
        allowNull: false,
    },
    film_id:{
        type: DataTypes.SMALLINT.UNSIGNED,
        primaryKey: true,
        allowNull: false
    },
    last_update:{
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
    }
    },
    {
        tableName: 'film_actor',
        timestamps: false,
        sequelize: connection
    }
)

export default FilmActor;